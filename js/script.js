localStorage.setItem('counterFlags', 0);
localStorage.setItem('counterClicks', 0);
localStorage.setItem('boardWidth', 8);
localStorage.setItem('boardHeight', 8);
const minesweeper = document.getElementsByClassName('minesweeper')[0];
const board = document.getElementsByClassName('game-area')[0];
let boardWidth = +localStorage.getItem('boardWidth');
let boardHeight = +localStorage.getItem('boardHeight');
let boardSize = boardWidth*boardHeight;
let minesQuantity = Math.floor(boardSize/6);
let counterFlags = document.querySelectorAll('.minesweeper-counter-flags')[0];
let counterMines = document.querySelectorAll('.minesweeper-counter-mines')[0];

function createBoard() {
    let boardWidth = +localStorage.getItem('boardWidth');
    let boardHeight = +localStorage.getItem('boardHeight');
    let boardSize = boardWidth*boardHeight;
    board.style.width = `${boardWidth*24}px`;
    board.style.height = `${boardHeight*24}px`;
    minesweeper.style.width =`${boardWidth*24 +30}px`;
    minesweeper.style.height =`${boardHeight*24 +110}px`;
    minesQuantity = Math.floor(boardSize/6);
    counterMines.innerText = `0/${minesQuantity}`;
    for (let i=0; i< boardSize; i++) {
        const newDiv = document.createElement('div');
        const divHidden = document.createElement('div');
        newDiv.className = 'game-item';
        divHidden.className = 'game-item-hidden';
        board.appendChild(newDiv);
        newDiv.appendChild(divHidden);
    }
    document.getElementsByClassName('fa-sync-alt')[0].classList.remove('active');
    document.getElementsByClassName('footer-text')[0].innerText = 'Началась новая игра';
}
function getPositionMines (){
    let i=0;
    let minesPosition = [];
    let boardWidth = localStorage.getItem('boardWidth');
    let boardHeight = localStorage.getItem('boardHeight');
    let boardSize = boardWidth*boardHeight;
    minesQuantity = Math.floor(boardSize/6);
    while(i<minesQuantity){
        let minePosition= Math.floor(Math.random()*boardSize)+1;
        function isEqual(num) {
            return num!== minePosition;
        }
        if (minesPosition.every(isEqual)) {
            i++;
            minesPosition.push(minePosition);
        }
    }
    return minesPosition;
}
function getMineCoords(pos) {
    let result =[];
    let boardWidth = +localStorage.getItem('boardWidth');
    let x = (pos % boardWidth) === 0 ? boardWidth : pos % boardWidth;
    let y = Math.ceil(pos/boardWidth);
    result=result.concat(x,y);
    return result;
}
function createMines(positionMines) {
    positionMines.forEach(function (item) {
        const newDiv = document.createElement('div');
        newDiv.className = 'mine';
        let gameItems =document.getElementsByClassName('game-item');
        gameItems[item - 1].appendChild(newDiv);
    });
}
function position(obj) {
    let result =[];
    let posX = obj.offsetParent.offsetTop;
    let posY = obj.offsetParent.offsetLeft;
    let y = (posX-1)/24 + 1;
    let x = (posY-1)/24 + 1;
    result.push({x : x, y : y});
    return result;
}
let isOutOf = function (x, y) {
    boardWidth =localStorage.getItem('boardWidth');
    boardHeight =localStorage.getItem('boardHeight');
    return x > boardWidth || y > boardHeight|| x < 0 || y < 0
};
let getNear = function (x, y) {
    let result = [];
    for (let dx = -1; dx <= 1; dx++) {
        for (let dy = -1; dy <= 1; dy++) {
            if (dx == 0 && dy == 0 || isOutOf(x +dx, y + dy) ) continue;
            if (x + dx > 0 && y + dy >0) result.push({x : x + dx, y : y + dy});
        }
    }
    return result
};
function getFirstClick() {
    let counterClicks = localStorage.getItem('counterClicks');
    if (!+counterClicks){
        document.getElementsByClassName('fa-sync-alt')[0].classList.add('active');
        document.getElementsByClassName('footer-text')[0].innerText = 'Начать игру заново ';
        localStorage.setItem('counterClicks', 1);
    }
}
function insertQuantity(n, elem) {
    const newDiv = document.createElement('div');
    let colorText =null;
    switch(n) {
        case 1:  colorText = 'one';
            break;
        case 2:  colorText = 'two';
            break;
        case 3:  colorText = 'three';
            break;
        case 4:  colorText = 'four';
            break;
        case 5:  colorText = 'five';
            break;
        case 6:  colorText = 'six';
            break;
        case 7:  colorText = 'seven';
            break;
    }
    newDiv.className = `quantity ${colorText}`;
    newDiv.innerText =`${n}`;
    elem.appendChild(newDiv);
}
function countNear (positionMines){
    let obj = document.querySelectorAll('.game-item-hidden');
    let objNumber = document.querySelectorAll('.game-item');
    let arr = [];
    let siblings = [];
    for (let i = 0; i<obj.length; i++) {
        arr =position(obj[i]);
        siblings = getNear(arr[0].x, arr[0].y);
        let n =siblings.filter(function (sibling) {
            return positionMines.filter(function (mine) {
                let mineCoords =getMineCoords(mine);
                return (mineCoords[0] === sibling.x && mineCoords[1] === sibling.y)
            }).length;
        }).length;
        if (n > 0 && !objNumber[i].children[1]) {
            insertQuantity(n, objNumber[i]);
        }
    }
}
let getPosition =(x, y) => (y-1)*boardWidth + x -1;
function openNear(x,y,e) {
    let gameItems  =document.getElementsByClassName('game-item');
    let mineHidden = [...document.getElementsByClassName('game-item-hidden')];
    getNear(x,y).forEach(function (sibling) {
        let pos = getPosition(sibling.x,sibling.y);
        if (gameItems[pos].children[1] && gameItems[pos].children[1].classList.contains('quantity') && !gameItems[pos].children[0].classList.contains('flag')){
            mineHidden[pos].hidden =true;
        }
        if (gameItems[pos].children[1] && gameItems[pos].children[1].classList.contains('mine') && !gameItems[pos].children[0].classList.contains('flag')) {
            mineHidden[pos].hidden =true;
            isMine(sibling.x,sibling.y);
        }
        else {
            if (!mineHidden[pos].hidden && !mineHidden[pos].classList.contains('flag'))  {
                mineHidden[pos].hidden =true;
                openNear(sibling.x, sibling.y,e)
            }
        }
    });
}
function openMines() {
    let mines = [...document.querySelectorAll('.mine')];
    mines.forEach(function (item) {
        item.previousSibling.hidden =true;
    })
}
function isMine(x,y) {
    let mineHidden = [...document.querySelectorAll('.game-item-hidden')];
    let pos = getPosition(x,y);
    let mine = mineHidden[pos].nextElementSibling;
    if ( mine && mine.classList.contains('mine') && !mineHidden[pos].classList.contains('flag') ){
        mine.parentNode.className = 'game-item active';
        mineHidden.forEach(function (item) {
            if (item.nextElementSibling && item.nextElementSibling.classList.contains('mine')) {
                item.hidden = true;
                document.getElementsByClassName('footer-text')[0].innerText = 'Вы проиграли ';
                document.getElementsByClassName('fa-sync-alt')[0].classList.add('active');
                document.removeEventListener('click', handlerClick);
                document.removeEventListener('contextmenu', handlerContextMenu);
                document.removeEventListener('dblclick', handlerDblclick);
            }
        });
        return true;
    }
    else return false;
}
function toggleFlag(target,quantityFlags) {
    target.classList.toggle('flag');
    counterFlags.innerText = `${quantityFlags}`;
    counterMines.innerText = `${quantityFlags}/${minesQuantity}`;
}
function countNearFlags(x, y) {
    let mineHidden = [...document.querySelectorAll('.game-item-hidden')];
    return getNear(x, y).filter(function (sibling) {
        let pos = getPosition(sibling.x,sibling.y);
        return mineHidden[pos].classList.contains('flag');
    }).length;
}
function toggleModal() {
    document.getElementById('modal').classList.toggle('open');
    document.querySelectorAll('.overlay')[0].classList.toggle('show');
}
function countHiddenItems() {
    let boardSize = boardWidth*boardHeight;
    let minesQuantity = Math.floor(boardSize/6);
    const quantityOpenedItems =[...document.querySelectorAll('.game-item-hidden')].filter(function (item) {
        return item.hidden
    }).length;
    if (quantityOpenedItems === boardSize - minesQuantity){
        openMines();
        document.getElementsByClassName('footer-text')[0].innerText = 'Вы выиграли! ';
    }
}
function removeBoard() {
    localStorage.setItem('counterFlags', 0);
    localStorage.setItem('counterClicks', 0);
    let gameItems = [...document.getElementsByClassName('game-item')];
    gameItems.forEach(function (item) {
        item.parentElement.removeChild(item);
    });

}
function validate (element1, element2){
    if (element1.classList.contains('warning') || element2.classList.contains('warning')) {
        document.querySelectorAll('.warning').forEach(function (item) {
            item.classList.remove('warning');
        });
        document.getElementById('form').removeChild(document.getElementById('form').children[5]);
    }
    if (isNaN(+element1.value)|| isNaN(+element2.value)|| +element1.value < 1 || +element2.value < 1) {
        if (isNaN(+element1.value)|| +element1.value < 1) element1.classList.add('warning');
        if (isNaN(+element2.value)|| +element2.value < 1) element2.classList.add('warning');
        const msgElem = document.createElement('p');
        msgElem.className = 'warning-text';
        msgElem.innerHTML = `Это не натуральное число`;
        document.getElementById('form').insertBefore(msgElem, document.querySelectorAll('.buttons')[0]);
    }
    else return true;
}
function sendSizeBoard() {
    localStorage.setItem('boardWidth', +form.elements[0].value);
    localStorage.setItem('boardHeight', +form.elements[1].value);
}
function handlerClick (e) {
    let target = e.target;
    if (target.id ==='open-modal') {
        e.preventDefault();
        toggleModal();
    }
    if (target.classList.contains('close')){
        toggleModal();
    }
    if (target.id ==='send-size') {
      if (validate(form.elements[0],form.elements[1])) {
          removeBoard();
          sendSizeBoard();
          toggleModal();
          createBoard();
          let positionMines =getPositionMines();
          createMines(positionMines);
          countNear(positionMines);
      }
      }


    while (target !== document){
        if (target.className === 'game-item-hidden'){
            getFirstClick();
            let arr =[];
            arr = position(target);
            isMine(arr[0].x, arr[0].y);
            if (target.parentNode.children.length === 1 ) {
                openNear(arr[0].x, arr[0].y,e);
                target.hidden=true;
            }
            else target.hidden =true;
            countHiddenItems();
            return;
        }
        target=target.parentNode;
    }

}
function handlerContextMenu (e){
    let target = e.target;
    e.preventDefault();
    let quantityFlags = localStorage.getItem('counterFlags');
    while (target !== document){
        if (target.classList.contains('game-item-hidden')) {
            getFirstClick();
            !target.classList.contains('flag') ? ++quantityFlags : --quantityFlags;
            localStorage.setItem('counterFlags', `${quantityFlags}`);
            toggleFlag(target,quantityFlags);
        }
        target=target.parentNode;
    }
}
function handlerDblclick(e) {
    let target = e.target;
    let arr = position(target);
    let quantityFlags = countNearFlags(arr[0].x, arr[0].y);
    let innerQuantity = target.innerText;
    while (target !== document){
        if (target.classList.contains('quantity') && quantityFlags == innerQuantity) {
            openNear(arr[0].x, arr[0].y,e);
            isMine(arr[0].x, arr[0].y);
        }
        target=target.parentNode;
    }
    countHiddenItems();
}
function handlerClickNewField (e) {
    let target = e.target;
    if (target.id ==='new-field') {
        e.preventDefault();
        localStorage.setItem('counterFlags', 0);
        localStorage.setItem('counterClicks', 0);
        counterFlags.innerText = `0`;
        removeBoard();
        createBoard();
        let positionMines = getPositionMines();
        createMines(positionMines);
        countNear(positionMines);
        document.getElementsByClassName('fa-sync-alt')[0].classList.remove('active');
        document.getElementsByClassName('footer-text')[0].innerText = 'Началась новая игра';
        document.addEventListener('click', handlerClick);
        document.addEventListener('contextmenu', handlerContextMenu);
        document.addEventListener('dblclick',handlerDblclick);
    }
}

createBoard();
let positionMines = getPositionMines();
createMines(positionMines);
countNear(positionMines);
document.addEventListener('click', handlerClick);
document.addEventListener('contextmenu', handlerContextMenu);
document.addEventListener('dblclick',handlerDblclick);
document.addEventListener('click', handlerClickNewField);

